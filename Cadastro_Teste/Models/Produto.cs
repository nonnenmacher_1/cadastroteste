﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Cadastro_Teste.Models
{
    public class Produto
    {
        public int ID { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "O nome deve ter entre 3 e 60 caracteres.")]
        public string Nome { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        [Range(0, 99999.99,ErrorMessage = "O Preço de Venda deve estar entre 0,00 e 99999,99.")]
        [Display(Name = "Preço")]
        [DataType(DataType.Currency)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:C}")]       
        public float Preco { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        public float Estoque { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        public string Fornecedor { get; set; }
    }

    public class ProdutosDBContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
    }
}