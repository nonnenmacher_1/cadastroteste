﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace Cadastro_Teste.Models
{
    public class Cliente
    {
        public int ID { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Cliente")]
        [StringLength(60, MinimumLength = 3, ErrorMessage = "O nome deve ter entre 3 e 60 caracteres.")]
        public string NomeEmpresa { get; set; }
        
        
        [Display(Name = "Última Compra")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        public DateTime UltimaCompra { get; set; }
        
        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "E-Mail")]        
        [EmailAddress(ErrorMessage = "E-mail em formato inválido.")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Campo obrigatório", AllowEmptyStrings = false)]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }
    }

    public class ClientesDBContext : DbContext
    {
        public DbSet<Cliente> Clientes { get; set; }
    }
}