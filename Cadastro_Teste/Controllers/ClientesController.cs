﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cadastro_Teste.Models;
using System.Text;

namespace Cadastro_Teste.Controllers
{
    public class ClientesController : Controller
    {
        private ClientesDBContext db = new ClientesDBContext();

        // GET: Clientes
        public ActionResult Index(string searchString)
        {
            var clientes = from m in db.Clientes
                           select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                clientes = clientes.Where(s => s.NomeEmpresa.Contains(searchString));
            }

            return View(clientes);

        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NomeEmpresa,UltimaCompra,Email,Endereco")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Clientes.Add(cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NomeEmpresa,UltimaCompra,Email,Endereco")] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cliente cliente = db.Clientes.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cliente cliente = db.Clientes.Find(id);
            db.Clientes.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Exportar()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("ID,NomeEmpresa,UltimaCompra,Email,Endereco \n");
            foreach (var dbItem in db.Clientes)
            {
                sb.Append(dbItem.ID);
                sb.Append(",");
                sb.Append(dbItem.NomeEmpresa);
                sb.Append(",");
                sb.Append(dbItem.UltimaCompra);
                sb.Append(",");
                sb.Append(dbItem.Email);
                sb.Append(",");
                sb.Append(dbItem.Endereco);
                sb.Append("\n");
            }
            HttpContext context = System.Web.HttpContext.Current;
            context.Response.Write(sb.ToString());
            context.Response.ContentType = "text/csv";
            context.Response.AddHeader("Content-Disposition", "attachment; filename = Clientes.csv");
            context.Response.End();
            return View();
        }
    }
}
